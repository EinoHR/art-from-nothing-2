var paused = new Boolean(false);

var path = new Path({
    segments: [new Point(view.center)],
    length: 1,
    strokeColor: new Color("#0e0d08"),
    strokeWidth: 2,
    strokeCap: 'round',
    closed: false
})

function onFrame(event) {
    var lastpoint = path.lastSegment.point;
    var point = new Point();
    point = randomPointInDistance(lastpoint, 100)
    path.add(point);
    path.reduce();
    path.smooth();
}

function randomPointInDistance(originpoint, distance) {
    newpoint = originpoint + ((new Point(distance, distance) * Point.random()) - (distance/2))
    return newpoint
}

function onMouseDrag(event) {
    var delta = event.downPoint.subtract(event.point)
    view.scrollBy(delta)
}   

function onKeyDown(event) {
    if (event.key == "+") {
        view.zoom += .5
    }
    if (event.key == "-") {
        if (view.zoom != .5) {
            view.zoom -= .5
        }
    }
    if (event.key == "space") {
        paused = !paused
    }
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1) + min);
}